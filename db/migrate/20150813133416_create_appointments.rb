class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.timestamp :start_time
      t.timestamp :final_time
      t.integer :consultant_id
      t.integer :customer_id

      t.timestamps null: false
      
    end
    add_foreign_key :appointments, :consultants
    add_foreign_key :appointments, :customers
  end
  
      
end
