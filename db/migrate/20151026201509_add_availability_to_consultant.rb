class AddAvailabilityToConsultant < ActiveRecord::Migration
  def change
    add_column :consultants, :availability, :text
  end
end
