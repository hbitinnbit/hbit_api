class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.integer :ranking
      t.text :comments
      t.integer :appointment_id

      t.timestamps null: false
    end
  end
end
