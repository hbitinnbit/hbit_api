class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :street
      t.string :zip
      t.float :latitude
      t.float :longitude
      t.integer :city_id
      t.integer :consultant_id

      t.timestamps null: false
    end
    
    add_foreign_key :addresses, :cities
  end
end
