class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :first_name,   null: false, default: ""
      t.string :last_name,    null: false, default: ""
      t.date :birth_date
      t.integer :genre
      t.string :phone
      
      t.timestamps null: false
    end
  end
end
