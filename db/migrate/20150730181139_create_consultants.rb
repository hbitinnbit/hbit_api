class CreateConsultants < ActiveRecord::Migration
  def change
    create_table :consultants do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :email_contact
      t.float :ranking, default: 0, null: false
      t.string :license
      t.integer :specialty_id

      t.timestamps null: false
    end
    
    add_foreign_key :consultants, :specialties
    add_foreign_key :addresses, :consultants
  end
end
