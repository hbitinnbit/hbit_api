class AddSurveyReceivedToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :survey_received, :boolean, null: false, default: false
  end
end
