class Api::V1::Consultants::AppointmentsController < ApplicationController
  
  before_action :authenticate_consultant!
  
  before_action :set_consultant
  before_action :set_appointment, only: [:update, :destroy]
  
  def index
    
    if params['month'].present?
      offset = params['offset'].to_i if params['offset'].present?
      offset ||= 0
      @appointments = @consultant.appointments.find_by_month(offset)
    else
      if params['offset'].present?
        offset = params['offset'].to_i 
        @appointments = @consultant.appointments.find_by_week(offset)
      else
          @appointments = @consultant.appointments.today
      end
    end
    render json: @appointments, each_serializer: AppointmentConsultantSerializer
  end


  def update
    if @appointment.nil?
      render json: {errors: {appointment: "Appointment not found"}, status: 'error'}, status: 422  
    elsif @appointment.update(appointment_params)
      render json: @appointment, serializer: AppointmentConsultantSerializer
    else
      render json: {errors: appointment.errors, status: 'error'}, status: 422  
    end
  end

  def destroy
    if @appointment.destroy(appointment_params)
    
    end
  end
  
  private 
  def set_consultant
    @consultant = current_consultant
  end
  
  def set_appointment
    @appointment = @consultant.appointments.where(id: params[:id]).first
  end 
  
  def appointment_params
    params.permit(:start_time, :final_time)
  end
  
end
