class Api::V1::Consultants::AddressesController < ApplicationController
  
  before_action :authenticate_consultant!
  
  before_action :find_address, only: [:update, :coordinates]  
  
  def update 
    city = address_params[:city]
    
    update_params = address_params.except(:city)
    update_params[:city_id] = city[:id] if city.present?
    
    if @address.update(update_params)
      render json: @address
    else
      render json: {errors: @address.errors, status: 'error'}, status: 422
    end 
  end
  
  def coordinates
     if @address.update(coordinates_params)
      render json: @address
    else
      render json: {errors: @address.errors, status: 'error'}, status: 422
    end  
  end
  
  private 
  def set_consultant
    @consultant = current_consultant
  end
  
  def find_address
    set_consultant
    @address = @consultant.address    
    if @address.nil? 
        @address =  Address.create(consultant: current_consultant)
    end
   end
  
  def address_params
    #params.permit(:street, :zip, :city_id)
    params.permit(:street, :zip, city: [:id])
  end
   
  def coordinates_params
    params.permit(:latitude, :longitude)
  end
  
end
