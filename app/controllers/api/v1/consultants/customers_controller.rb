class Api::V1::Consultants::CustomersController < ApplicationController
  
  before_action :authenticate_consultant!
  
  before_action :set_customer, only: [:show]  
  
  def show
    expires_in 1.hour, must_revalidate: true
    render json: @customer
  end
  
  private
  def set_customer
    id = params[:id]
    #@customer = current_consultant.appointments.where(id: :id).pluck('customer').first
    @customer = Customer.find(id)
  end
end
