class Api::V1::Consultants::AvatarsController < ApplicationController
  
  before_action :authenticate_consultant!
  
  def update
    
    @consultant=current_consultant
    @consultant.avatar=avatar_params['avatar']
    
    if @consultant.save
      data = ActiveModel::SerializableResource.new(@consultant.avatar).serializable_hash
      render json: {data: data}, status: 200
    else
      render json: {errors: @consultant.errors, status: 'error'}, status: 422
    end
    
  end
  
  private
  
  def avatar_params
    params.permit(:avatar)
  end

  
end
