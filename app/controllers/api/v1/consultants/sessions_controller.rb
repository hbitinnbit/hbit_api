class Api::V1::Consultants::SessionsController < DeviseTokenAuth::SessionsController
    
    def render_create_success
       data = ActiveModel::SerializableResource.new(@resource).serializable_hash
       render json: {data:  data } 
    end
end
