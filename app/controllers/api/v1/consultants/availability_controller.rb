class Api::V1::Consultants::AvailabilityController < ApplicationController
  
  before_action :authenticate_consultant!
  
  before_action :set_consultant
  
  def update
    @consultant.availability=availability_params['availability']
    if @consultant.save
      render json: {data: []}, status: 200
    else
      render json: {errors: @consultant.errors, status: 'error'}, status: 422
    end  
  end
  
  def show
    render json: @consultant, serializer: AvailabilitySerializer
  end
  
  private
  def availability_params
    params.permit(:availability)
  end

  def set_consultant
    @consultant=current_consultant   
  end
end
