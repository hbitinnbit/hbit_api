class Api::V1::Consultants::SpecialtiesController < ApplicationController
  
  before_action :authenticate_consultant!
  
  before_action :set_specialty, only: [:update]  
  
  def index
    expires_in 1.week
    @specialties = Specialty.all
    render json: @specialties
  end
  
  def update
    @consultant = current_consultant
    #@consultant = Consultant.first
    @consultant.specialty = @specialty
    if @consultant.save
      render json: @consultant
    else
      render json: {errors: @consultant.errors, status: 'error'}, status: 422
    end
  end
  
  private 
  def set_specialty
    @specialty = Specialty.find(params[:id])
  end
end
