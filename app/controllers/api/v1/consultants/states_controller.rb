class Api::V1::Consultants::StatesController < ApplicationController
  
  before_action :authenticate_consultant!
  
  before_action :set_state, only: [:cities] 
  
  def index
    expires_in 1.week
    @states = State.all
    render json: @states
  end

  def cities
    expires_in 1.week
    render json: @state.cities
  end
  
  private
  def set_state
    @state = State.find(params[:id])
  end
  
end
