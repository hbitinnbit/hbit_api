class Api::V1::Customers::AppointmentsController < ApplicationController
  
  before_action :authenticate_customer!
  
  before_action :set_consultant, only: [:create, :update, :destroy]
  before_action :set_appointment, only: [:update, :destroy, :show, :surveys]
  
  
  def index
     offset = params['offset'].to_i if params['offset'].present?
     offset ||= 0
    if params['consultant_id'].present?
      consultant = Consultant.find(params['consultant_id'].to_i)
      if params['week'].present?
        @appointments = consultant.appointments.find_by_week(offset)     
      else
        @appointments = consultant.appointments.find_by_month(offset)     
      end
    else
      if params['next'].present?
        @appointments = current_customer.appointments.next
      else
        @appointments = current_customer.appointments.find_by_month(offset)
      end
    end
    render json: @appointments
  end

  def create
    appointment = Appointment.new(appointment_params.merge(customer: current_customer, consultant: @consultant))
    if appointment.save
      render json: appointment
    else
      render json: {errors: appointment.errors, status: 'error'}, status: 422
    end
  end

  def update
    if @appointment.update(appointment_params)
      render json: appointment
    else
      render json: {errors: appointment.errors, status: 'error'}, status: 422  
    end
  end

  def destroy
    if @appointment.destroy(appointment_params)
    
    end
  end
  
  def surveys
    @survey = Survey.new(survey_params)
    @survey.appointment = set_appointment
    if @survey.save
      @appointment.update(survey_received: true)
      render json: @survey
    else
      render json: {errors: @survey.errors, status: 'error'}, status: 422  
    end
  end
  
  private 
  def set_consultant
    @consultant=Consultant.find(params[:consultant_id])
  end
  
  def set_appointment
    #@appointment=Appointment.find(params[:id])
    @appointment = current_customer.appointments.where(id: params[:id]).first
  end 
  
  def appointment_params
    params.permit(:start_time, :final_time)
  end
  
    def survey_params
    params.permit(:ranking, :comments)
  end
  
end
