class Api::V1::Customers::ConsultantsController < ApplicationController
  
  before_action :authenticate_customer!
  
  before_action :set_consultant, only: [:show, :availability]  
  
  api :GET, "/customers/consultants/(page/:page)(/per/:per)", "Search for consultants"
  def index
    expires_in 10.minutes, must_revalidate: true
    if params[:name]
      @consultants=Consultant.search_by_name(params[:name]).page(params[:page]).per(params[:per])
    elsif params[:specialty_id]
      @consultants=Consultant.search_by_specialty_id.page(params[:specialty_id]).per(params[:per])
    elsif params[:specialty]
      @consultants=Consultant.search_by_specialty_description(params[:specialty]).page(params[:page]).per(params[:per])
    else
      @consultants=[]
    end 
    render json: @consultants
  end

  api! 'Find a consultant and its details'
  def show
    fresh_when(etag: @consultant, last_modified: @consultant.updated_at, public: true)
    render json: @consultant
  end
  
  def locations
    expires_in 10.minutes, must_revalidate: true
    if params[:name]
      @consultants=Consultant.search_by_location(params[:latitude],params[:longitude],params[:radio]).search_by_name(params[:name])
    elsif params[:specialty_id]
      @consultants=Consultant.search_by_location(params[:latitude],params[:longitude],params[:radio]).search_by_specialty_id(param[:specialty_id])
    elsif params[:specialty]
      @consultants=Consultant.search_by_location(params[:latitude],params[:longitude],params[:radio]).search_by_specialty_description(params[:specialty])
    else
      @consultants=Consultant.search_by_location(params[:latitude],params[:longitude],params[:radio])
    end 
    render json: @consultants
  end
  
  def availability
    expires_in 1.hour
    render json: @consultant, serializer: AvailabilitySerializer
  end
  
  
  private 
  def set_consultant
    @consultant=Consultant.find(params[:id])
  end
  
end
