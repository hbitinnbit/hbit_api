class Api::V1::Customers::SessionsController < DeviseTokenAuth::SessionsController
    
    def render_create_success
       data = ActiveModel::SerializableResource.new(@resource).serializable_hash
       render json: {data:  data } 
    end

    def create
      if params['fbtoken'].present?
        login_facebook 
      else 
        super
      end
    end
    
    private 
    def login_facebook
      token = params['fbtoken']
      begin
        @graph = Koala::Facebook::API.new(token)
        user = @graph.get_object("me",{fields: "email,first_name,last_name,birthday,gender"})
        @resource = find_or_create(user,'facebook')
        if @resource.persisted?
          update_auth_headers
          render_create_success
        else
          render_create_error_bad_credentials
        end
      rescue => exception
        puts exception.message
        render_create_error_bad_credentials
      end
    end
    
    def update_auth_headers
      @client_id = SecureRandom.urlsafe_base64(nil, false)
      @token = SecureRandom.urlsafe_base64(nil, false)
      @resource.tokens[@client_id] = {
        token: BCrypt::Password.create(@token),
        expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
      }
      @resource.save
      sign_in(:user, @resource, store: false, bypass: false)
    end
    
    def find_or_create(user, provider)      
      resource = Customer.find_by_provider(user["email"], provider).first
      if resource.nil?
        resource = create_from_social_network(user, provider)
      end 
      return resource
    end
    
    def create_from_social_network(user, provider)
      user["birth_date"] = user.delete "birthday"
      user["genre"] = user.delete "gender"
      resource = Customer.new(user.except('id'))
      resource.uid = user["email"]
      resource.provider = provider
      resource.password = SecureRandom.hex
      resource.save
      return resource
    end
    
end
