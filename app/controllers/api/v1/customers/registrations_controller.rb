class Api::V1::Customers::RegistrationsController < DeviseTokenAuth::RegistrationsController
    
    def render_create_success
      data = ActiveModel::SerializableResource.new(@resource).serializable_hash
      render json: {data:  data, status: 'success' } 
    end
    
    def render_update_success
      data = ActiveModel::SerializableResource.new(@resource).serializable_hash
      render json: {data:  data , status: 'success'} 
    end
    
    def render_update_error
      render json: {errors: @resource.errors, status: 'error'}, status: 422  
    end
    
    def render_create_error
      render json: {errors: @resource.errors, status: 'error'}, status: 422
    end
    
end