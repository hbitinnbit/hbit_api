class Api::V1::Customers::AvatarsController < ApplicationController
  
  before_action :authenticate_customer!
  
  def update
    
    @customer=current_customer
    @customer.avatar=avatar_params['avatar']
    
    if @customer.save
      data = ActiveModel::SerializableResource.new(@customer.avatar).serializable_hash
      render json: {data: data}, status: 200
    else
      render json: {errors: @customer.errors, status: 'error'}, status: 422
    end
    
  end
  
  private
  
  def avatar_params
    params.permit(:avatar)
  end

  
end
