class ApplicationController < ActionController::API
  
  #protect_from_forgery with: :null_session
  include DeviseTokenAuth::Concerns::SetUserByToken
  before_action :configure_permitted_parameters, if: :devise_controller?
  respond_to :json
  
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up).push(:first_name,:last_name,:genre,:birth_date, :phone, :email_contact, :license)
    devise_parameter_sanitizer.for(:account_update).push(:first_name,:last_name,:genre,:birth_date, :phone, :email_contact, :license)
  end
  
end
