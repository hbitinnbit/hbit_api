class AvatarUploaderSerializer < ActiveModel::Serializer
  attributes :url, :thumb, :icon
  
  def thumb
    object.thumb.url
  end
  
  def icon
    object.icon.url
  end
  
end
