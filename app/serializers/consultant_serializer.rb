class ConsultantSerializer < ActiveModel::Serializer
  
  attributes :id, :email, :first_name, :last_name, :phone, :email_contact, :license, :ranking

  has_one :address
  
  belongs_to :specialty
  
  has_one :avatar
  
end
