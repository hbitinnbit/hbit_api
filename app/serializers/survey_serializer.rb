class SurveySerializer < ActiveModel::Serializer
  attributes :id, :ranking, :comments
end
