class CustomerSerializer < ActiveModel::Serializer
  attributes :id, :email, :first_name, :last_name, :genre, :birth_date, :phone
  
  has_one :avatar
  
end
