class SpecialtySerializer < ActiveModel::Serializer
  attributes :id, :description
end
