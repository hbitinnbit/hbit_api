class AppointmentConsultantSerializer < ActiveModel::Serializer
  attributes :id, :start_time, :final_time, :customer
  
  #belongs_to :customer
  def customer
    ActiveModel::SerializableResource.new(object.customer).serializable_hash
  end
end
