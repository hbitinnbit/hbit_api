class AppointmentSerializer < ActiveModel::Serializer
  attributes :id, :start_time, :final_time, :survey_received, :consultant
  
  def consultant
    ActiveModel::SerializableResource.new(object.consultant).serializable_hash
  end
  
end
