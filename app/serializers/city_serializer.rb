class CitySerializer < ActiveModel::Serializer
  attributes :id, :name, :state
  
  def state
    StateSerializer.new(object.state).attributes
  end
  
end
