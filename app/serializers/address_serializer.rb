class AddressSerializer < ActiveModel::Serializer  
  attributes :id, :street, :zip, :coordinates, :city
  
  def city
    #object.city.name
    CitySerializer.new(object.city).attributes
  end
  
end
