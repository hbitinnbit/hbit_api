class AvailabilitySerializer < ActiveModel::Serializer
  attributes :data
  
  def data
    object.availability.split("\n")
  end
end
