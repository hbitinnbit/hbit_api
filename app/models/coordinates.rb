class Coordinates
  attr_accessor :longitude, :latitude
  
  def initialize(latitude, longitude)
    self.longitude=longitude
    self.latitude=latitude
  end
  
end