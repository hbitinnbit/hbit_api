class Survey < ActiveRecord::Base
  
  validates :ranking, numericality: { only_integer: true, greater_than: 0, less_than_or_equal_to: 5 }, presence: true
  
  validates :appointment, presence: true
  
  belongs_to :appointment
  
end
