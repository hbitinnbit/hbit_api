class City < ActiveRecord::Base
   validates :name,:state, presence: true
   belongs_to :state
   has_many :addresses
end
