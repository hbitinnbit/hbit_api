class Customer < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :trackable, :validatable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  validates :first_name, :last_name, :email, presence: true
  
  mount_base64_uploader :avatar, AvatarUploader
  
  has_many :appointments
  
  GENRE_OPTIONS= {male:0, female: 1}
  enum genre: GENRE_OPTIONS
  
  scope :find_by_provider, ->(uid, provider){ where(uid: uid, provider: provider)}

  
  
end
