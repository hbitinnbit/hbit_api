class Consultant < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable,  :trackable, :validatable, :omniauthable
          
          
  include DeviseTokenAuth::Concerns::User

  include PgSearch

  validates :first_name, :last_name, presence: true
  
  mount_base64_uploader :avatar, AvatarUploader

  has_one :address
  
  has_many :appointments

  belongs_to :specialty  
  

  scope :search_by_specialty_id, ->(id) { where(specialty_id: id)}
  
  pg_search_scope :search_by_specialty_description,
                  :associated_against => {
                    :specialty => [:description]
                  },
                  :using => {
                    :tsearch => {:prefix => true}
                  }

  pg_search_scope :search_by_name,
                  :against => [:first_name, :last_name],
                  :using => {
                    :tsearch => {:prefix => true}
                  }
    
                  
  def self.search_by_location(latitude, longitude, radio=0.0009)
    latitude=latitude.to_f
    longitude=longitude.to_f
    radio=radio.to_f
    latitudes=[latitude-radio, latitude+radio]
    longitudes=[longitude-radio, longitude+radio]
    
    Consultant.joins(:address).
      where('addresses.latitude' => latitudes.first .. latitudes.last)
      .where('addresses.longitude' => longitudes.first .. longitudes.last)
  end
  
end
