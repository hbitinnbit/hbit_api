class State < ActiveRecord::Base
  validates :name,:short_name, presence: true
  has_many :cities
end
