class Appointment < ActiveRecord::Base

  include IceCube
  
  after_initialize :normalize_dates

  validates :start_time,:final_time, :consultant, :customer, presence: true

  validate :validate_overlaps, :validate_dates #TODO Activate :validate_availability, 
  
  validate :validate_not_in_past

  belongs_to :consultant
  belongs_to :customer
  
  has_one :survey
  
  scope :exclude_id, ->(id) {where.not(id: id) if id.present?}
  scope :after_start_time, ->(time)  {where('start_time < ?', time) if time.present?}
  scope :before_final_time, ->(time) {where('final_time > ?', time) if time.present?}
  scope :time_between, ->(time) { after_start_time(time).before_final_time(time) }
  scope :contains_another, ->(start,final){ where('? <= start_time', start).where( '? >= final_time', final)  }
  
  
  scope :find_by_month, ->(offset=0){
    start = Time.now.at_beginning_of_month + offset.months
    final = Time.now.end_of_month + offset.months
    #puts "fuck #{start} #{final}"
    where('start_time BETWEEN ? and ?',start, final)
  }
  
  scope :next, ->{
    where('start_time >= ?', Time.now)
  }
  
  scope :today, ->{
    start = Time.now.at_beginning_of_day
    final = Time.now.end_of_day
    #puts "today #{start} #{final}"
    where('start_time BETWEEN ? and ?',start, final)
  }
  
  scope :find_by_week, ->(offset = 0){
    start = Time.now.at_beginning_of_week + offset.weeks
    final = Time.now.end_of_week + offset.weeks
    #puts ":find_by_week #{offset} #{start} #{final}"
    where('start_time BETWEEN ? and ?',start, final)
  }
  
  def within_availability?

    #puts consultant.nil?
    #puts consultant.availability.nil?

    return false if consultant.nil? || consultant.availability.nil?

    schedule=Schedule.from_ical(consultant.availability)
    
    #puts consultant.availability
    
    occurrence =  schedule.occurrences_between(start_time, final_time).first
    
    #puts "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    #puts schedule.occurs_between?(start_time, final_time)
    #puts ">>>>>>>>>>>>>> start_time: #{start_time} | final_time: #{final_time}"
    
    
    
    #puts schedule.occurrences(Time.now.change(month: 12, day: 31))
    
    appointment_interval = Occurrence.new(start_time, final_time)
    b = false;    
    unless occurrence.nil?
      b = occurrence.intersects?(appointment_interval) && 
        appointment_interval.duration < occurrence.duration &&
        occurrence.start_time <= appointment_interval.start_time &&
        occurrence.end_time > appointment_interval.end_time
    
    
        #puts "occurrence.intersects?(appointment_interval) #{occurrence.intersects?(appointment_interval)}" 
        #puts "appointment_interval.duration < occurrence.duration #{appointment_interval.duration < occurrence.duration}"
        #puts "occurrence.start_time <= appointment_interval.start_time #{occurrence.start_time} - #{appointment_interval.start_time}: #{occurrence.start_time <= appointment_interval.start_time}"
        #puts "occurrence.end_time > appointment_interval.end_time #{occurrence.end_time > appointment_interval.end_time}"
    end
    
    return b;

  end  

  def does_not_overlap_other_appoinments?
    return false if consultant.nil? || consultant.availability.nil?
    count=[
      consultant.appointments.exclude_id(self.id).time_between(self.start_time).count,
      consultant.appointments.exclude_id(self.id).time_between(self.final_time).count,
      consultant.appointments.exclude_id(self.id).contains_another(self.start_time, self.final_time).count
    ]    
    #puts ">>>>>>>>>>> self id: #{id}| count #{count} | #{self.start_time} | #{self.final_time}"
    #puts ">>>>>>>>>>> start_time between #{consultant.appointments.exclude_id(self.id).time_between(self.start_time).pluck(:id, :start_time, :final_time)}"
    #puts ">>>>>>>>>>> final_time between #{consultant.appointments.exclude_id(self.id).after_start_time(self.final_time).pluck(:id, :start_time, :final_time)}"
    #puts ">>>>>>>>>>> start_time between #{consultant.appointments.exclude_id(self.id).where('start_time >= ?', self.start_time).pluck(:id, :start_time, :final_time)}"
    #puts ">>>>>>>>>>> final_time between #{consultant.appointments.exclude_id(self.id).where('start_time <= ?', self.final_time).pluck(:id, :start_time, :final_time)}"
    count.inject{|sum,x| sum + x } == 0
  end
  
  
  
  private
  def normalize_dates
    self.start_time = start_time.change(sec: 0) if start_time.present?
    self.final_time = final_time.change(sec: 0) if final_time.present?
  end
  
  def validate_availability
    unless within_availability?
       errors.add(:start_time, "There is not available time")
    end
  end
  
  def validate_overlaps
    unless does_not_overlap_other_appoinments?
      errors.add(:start_time, "There are other scheduled appoinments")
    end
  end
  
  def validate_dates
    if final_time.nil? || start_time.nil?
      return
    end
    
    if final_time < start_time
      errors.add(:final_time, "The final time may not be before the starting time")
    end    
  end

  def validate_not_in_past
    if  start_time.present? && start_time < Time.now 
      errors.add(:final_time, "The final time must not be in the past")
    end 
  end

end
