class Address < ActiveRecord::Base
  
  validates :street, :zip, :city, presence: true
  
  
  belongs_to :city
  belongs_to :consultant
  
  def coordinates
    Coordinates.new(latitude, longitude)
  end
  
end
