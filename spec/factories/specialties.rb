FactoryGirl.define do
  factory :specialty do
    description { ["Orthopedy","Anesthesiology","Pediatric Cardiology","Obstetrics & Gynecology"].shuffle.first}
    
    factory :invalid_specialty do
      description nil
    end
  end
end
