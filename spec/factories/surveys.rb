FactoryGirl.define do
  factory :survey do
    ranking {[1,2,3,4,5].shuffle.first}
    comments {FFaker::Lorem.paragraphs}
    association :appointment, factory: :appointment
    factory :invalid_survey do
      ranking {-1}
      appointment nil
    end
  
  end
  
end
