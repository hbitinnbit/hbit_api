FactoryGirl.define do
  factory :state do
    name {FFaker::AddressMX.state}
    short_name {FFaker::AddressMX.state_abbr}

    factory :invalid_state do
      name nil
      short_name nil
    end

  end

end
