FactoryGirl.define do
  factory :appointment do
    start_time {DateTime.now.change(hour:10, minutes:0)+1.day}
    final_time {DateTime.now.change(hour:10, minutes:30)+1.day}
    association :consultant, factory: :consultant
    association :customer, factory: :customer
    
    factory :invalid_appointment do
      start_time nil
      final_time nil
      consultant nil
      customer nil  
    end
  end
end
