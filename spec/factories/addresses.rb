FactoryGirl.define do
  factory :address do
    street {FFaker::Address.street_address}
    zip {FFaker::AddressMX.zip_code}
    latitude {FFaker::Geolocation.lat}
    longitude {FFaker::Geolocation.lng}
    association :city, factory: :city
  
    factory :invalid_address do
      street nil
      zip nil
      city nil
    end
  end
end
