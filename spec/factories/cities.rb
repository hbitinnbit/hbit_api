FactoryGirl.define do
  factory :city do
    name {FFaker::Address.city}
    association :state, factory: :state
    
    factory :invalid_city do
      name nil
      state nil
    end
  end
end
