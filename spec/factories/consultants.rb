FactoryGirl.define do
  factory :consultant do
    first_name {FFaker::Name.first_name}
    last_name {FFaker::Name.last_name}
    phone {FFaker::PhoneNumber.phone_number}
    email_contact {FFaker::Internet.free_email}
    association :specialty, factory: :specialty
    association :address, factory: :address
    email { FFaker::Internet.email }
    password "12345678"
    password_confirmation "12345678"
    provider 'hbit'
    uid {SecureRandom.uuid}
    license 2555
    ranking 4.5
    availability {File.read(File.join(Rails.root,'spec/fixtures/',"basic.ics"))}
    
    
    factory :invalid_consultant do
      first_name nil
      last_name nil
      email nil
    end    
  end
end
