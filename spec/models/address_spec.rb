require 'spec_helper'

describe Address do
   describe 'factories' do
    it 'has a valid factory' do
      expect(build(:address)).to be_valid
    end
    it 'has an invalid factory' do
      expect(build(:invalid_address)).to_not be_valid
    end
  end
  describe 'attributes' do
    subject { build(:address) }
    it { should respond_to :street }
    it { should respond_to :zip }
    it { should respond_to :latitude }
    it { should respond_to :longitude }
    it { should belong_to :city }
    it { should belong_to :consultant }
  end
  
  describe 'coordinates' do
    it 'return object' do
      address=build(:address)
      coordinates=address.coordinates
      expect(address.longitude).to be_eql(coordinates.longitude)
      expect(address.latitude).to be_eql(coordinates.latitude)      
    end
  end
end
