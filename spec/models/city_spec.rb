require 'spec_helper'

describe City do
 describe 'factories' do
    it 'has a valid factory' do
      expect(build(:city)).to be_valid
    end
    it 'has an invalid factory' do
      expect(build(:invalid_city)).to_not be_valid
    end
  end
  describe 'attributes' do
    subject { build(:city) }
    it { should respond_to :name }
    it { should respond_to :state }
    it { should belong_to :state }
  end
end
