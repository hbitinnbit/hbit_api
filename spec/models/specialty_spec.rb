require 'spec_helper'

describe Specialty do
  describe 'factories' do
    it 'has a valid factory' do
      expect(build(:specialty)).to be_valid
    end
    it 'has an invalid factory' do
      expect(build(:invalid_specialty)).to_not be_valid
    end
  end
  describe 'attributes' do
    subject { build(:specialty) }
    it { should respond_to :description }
  end
end
