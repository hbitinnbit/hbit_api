require 'spec_helper'

describe Consultant do
  describe 'factories' do
    it 'has a valid factory' do
      expect(build(:consultant)).to be_valid
    end
    it 'has an invalid factory' do
      expect(build(:invalid_consultant)).to_not be_valid
    end
  end
  describe 'attributes' do
    subject { build(:consultant) }
    it { should respond_to :first_name }
    it { should respond_to :last_name }
    it { should respond_to :phone }
    it { should respond_to :email_contact }
    it { should have_one :address }
    it { should belong_to :specialty }
    it { should have_many :appointments }
  end

  describe 'search' do
    before(:each) do
      @consultants=Array.new
      @orthopedia=create(:specialty, description: 'Orthopedia')
      @cardiology=create(:specialty, description: 'Cardiology')

      #Referencia 19.4098668,-99.1628447

      addresses=[
        #Roma-Condesa
        build(:address, latitude: 19.4098668,longitude: -99.1628447),
        build(:address, latitude: 19.4066594, longitude: -99.1550475),
        build(:address, latitude: 19.2886886, longitude: -99.1485177),
        build(:address, latitude: 19.4055563, longitude: -99.1676858),
        build(:address, latitude: 19.4144413, longitude: -99.1507759),
        #Centro
        build(:address, latitude: 19.2892312, longitude: -99.1623643),
        build(:address, latitude: 19.4052578, longitude: -99.1650948),
        build(:address, latitude: 19.4104821, longitude: -99.1849511),
        build(:address, latitude: 19.2892312, longitude: -99.1623643),
        build(:address, latitude: 19.4128673, longitude: -99.1743645) ]

      5.times{
        @consultants << create(:consultant, specialty: @cardiology)
      }
      5.times{
        @consultants << create(:consultant, specialty: @orthopedia)
      }

      index=0;
      @consultants.each do |consultant|
        addresses[index].consultant=consultant
        addresses[index].save
        index+=1
      end

    end

    describe 'by specialty id' do
      it 'with value' do
        consultants=Consultant.search_by_specialty_id(@orthopedia.id);
        expect(consultants.count).to eq(5)
      end

      it 'no params' do
        consultants=Consultant.search_by_specialty_id(nil);
        expect(consultants.count).to eq(0)
      end
    end

    describe 'by specialty description' do
      it 'partial description' do
        consultants=Consultant.search_by_specialty_description('ortho');
        expect(consultants.count).to eq(5)
      end

      it 'no params' do
        consultants=Consultant.search_by_specialty_description(nil);
        expect(consultants.count).to eq(0)
      end
    end

    describe 'by name' do
      it ', first name' do
        name=@consultants.first.first_name
        consultants=Consultant.search_by_name(name);
        expect(consultants.count).to_not eq(0)
      end

      it ', last name' do
        name=@consultants.last.last_name
        consultants=Consultant.search_by_name(name);
        expect(consultants.count).to_not eq(0)
      end
    end

    describe 'by location' do

      it 'default radio' do
        consultants=Consultant.search_by_location(19.4098668,-99.1628447)
        expect(consultants.count).to eq(1)
      end

      it 'within a radio' do
        consultants=Consultant.search_by_location(19.4098668,-99.1628447, 0.1)
        expect(consultants.count).to eq(7)
      end

      it 'within a radio and strings' do
        consultants=Consultant.search_by_location('19.4098668','-99.1628447', '0.1')
        expect(consultants.count).to eq(7)
      end
      
      it 'within a radio and invalid strings' do
        consultants=Consultant.search_by_location('qweqweeqwqwe','123213123132', 'qeqwweq')
        expect(consultants.count).to eq(0)
      end


      it 'within a radio with other params' do
        consultants=Consultant.search_by_location(19.4098668,-99.1628447, 0.1).search_by_specialty_description('ortho')
        expect(consultants.count).to eq(3)

      end
    #19.4059180,-99.1686997 ;
    #19.4066922,-99.1620103
    #default offset 0.000774
    end
  end
end
