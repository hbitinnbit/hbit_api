require 'spec_helper'

RSpec.describe Appointment, type: :model do
  describe 'factories' do
    it 'has a valid factory' do
      expect(build(:appointment)).to be_valid
    end
    it 'has an invalid factory' do
      expect(build(:invalid_appointment)).to_not be_valid
    end
  end
  describe 'attributes' do
    subject { build(:appointment) }
    it { should respond_to :start_time }
    it { should respond_to :final_time }
    it { should belong_to :consultant }
    it { should belong_to :customer }
  end
  
  describe 'find by ' do
    let(:base_date){
      DateTime.now.beginning_of_month.change(hour:10, minutes:00, seconds:00)+1.week
    }
    
    let(:consultant){
      create(:consultant) 
    }
    
    let(:customer){
      create(:customer)
    }
    
    let(:now){
      Time.now
    }
    
    let(:some_weeks_ago_base){
      Time.now - 12.weeks
    }
    
    before(:each) do
      #Today
      build(:appointment,start_time: now, final_time: (now + 1.hour), consultant: consultant, customer: customer ).save(validate: false)
      
      #Some weeks ago
      build(:appointment,start_time: some_weeks_ago_base, final_time: (some_weeks_ago_base + 1.hour), consultant: consultant, customer: customer ).save(validate: false)
      
      
      build(:appointment,start_time: base_date - 1.month, final_time: (base_date + 1.hour - 1.month), consultant: consultant, customer: customer ).save(validate: false)
     
      build(:appointment, start_time: base_date, final_time: (base_date + 1.hour), consultant: consultant, customer: customer ).save(validate: false)
      build(:appointment, start_time: base_date + 1.month, final_time: (base_date + 1.hour + 1.month), consultant: consultant, customer: customer ).save(validate: false)
      
      build(:appointment, start_time: base_date + 1.week, final_time: (base_date + 1.hour + 1.week), consultant: consultant, customer: customer ).save(validate: false)
    end
    
    it 'current month' do
      appointments = Appointment.find_by_month
      expect(appointments.count).to eq 3
    end
    
    it 'last month' do
      
      #puts Appointment.pluck('start_time')
      
      appointments = Appointment.find_by_month(-1)
      expect(appointments.count).to eq 1
    end
    
    it 'next month' do
      appointments = Appointment.find_by_month(1)
      expect(appointments.count).to eq 1
    end
    
    it 'four months ago' do
      appointments = Appointment.find_by_month(-4)
      expect(appointments.count).to eq 0
    end
    
    it 'three months further' do
      appointments = Appointment.find_by_month(3)
      expect(appointments.count).to eq 0
    end
    
    it 'next' do
      appointments = Appointment.next
      expect(appointments.count).to_not eq 0
    end
    
    it 'today' do
      appointments = Appointment.today
      expect(appointments.count).to_not eq 0
    end
    
    it 'this week' do
      appointments = Appointment.find_by_week
      expect(appointments.count).to_not eq 0
    end
    
#    it 'next week' do
#      appointments = Appointment.find_by_week(1)
#      expect(appointments.count).to_not eq 0
#    end
    
    it 'six weeks ago' do
      appointments = Appointment.find_by_week(-12)
      expect(appointments.count).to_not eq 0
    end
  end
  
  
  describe 'within_availability' do
    let(:base_date){
      DateTime.now.beginning_of_week.change(hour:10, minutes:00, seconds:00)+1.week
    }
    
    let(:consultant){
      create(:consultant) 
    }
    
    let(:customer){
      create(:customer)
    }
    
    
    it 'valid' do
      #a=create(:appointment, start_time: base_date, final_time: (base_date + 1.hour), consultant: consultant, customer: customer )
      #puts "chelas: >>> #{base_date}"
      a=create(:appointment, start_time: (base_date), final_time: (base_date + 2.hour), consultant: consultant, customer: customer )
      expect(a.within_availability?).to be_truthy
    end
    it 'do not intersect' do
      invalid_time=base_date.change(hour:21)
      a=build(:appointment, start_time: invalid_time, final_time: (invalid_time + 1.hour), consultant: consultant, customer: customer )
      expect(a.within_availability?).to be_falsey
    end
    
    it 'bigger than availability' do
      invalid_time=base_date.change(hour:11)
      a=build(:appointment, start_time: invalid_time, final_time: (invalid_time + 10.hour), consultant: consultant, customer: customer )
      expect(a.within_availability?).to be_falsey
    end
    
    it 'bigger than availability2' do
      invalid_time=base_date.change(hour:8)
      a=build(:appointment, start_time: invalid_time, final_time: (invalid_time + 12.hour), consultant: consultant, customer: customer )
      expect(a.within_availability?).to be_falsey
    end
    
    it 'bigger than availability3' do
      invalid_time=base_date.change(hour:8)
      a=build(:appointment, start_time: invalid_time, final_time: (invalid_time + 2.hour), consultant: consultant, customer: customer )
      expect(a.within_availability?).to be_falsey
    end
    
  end
  
  describe 'normalize_dates' do
    let(:base_date){
      DateTime.now.change(hour:10, minutes:00, seconds: 15)+1.day
    }
    it 'dates' do
      a = build(:appointment, start_time: base_date, final_time: (base_date + 1.hour))
      #puts a.inspect
      expect(a.start_time.sec).to eq 0
      expect(a.final_time.sec).to eq 0  
      #puts a.inspect
    end
  end
  
  describe 'does_not_overlap_other_appoinments' do
       
    let(:base_date){
      DateTime.now.change(hour:10, minutes:00, seconds:00)+1.day
    }
    
    let(:consultant){
      create(:consultant) 
    }
    
    let(:customer){
      create(:customer)
    }
    
    before(:each) do
      create(:appointment, start_time: base_date, final_time: (base_date + 1.hour), consultant: consultant, customer: customer )
    end
    
    it 'valid' do
      appointment=build(:appointment, start_time: (base_date+15.minutes+1.hour), final_time: (base_date+45.minutes+1.hour), consultant: consultant, customer: customer)
      expect(appointment.does_not_overlap_other_appoinments?).to be_truthy
      expect(appointment.valid?).to be_truthy      
    end
    
    it 'starts when previous have just finished' do
      appointment=build(:appointment, start_time: (base_date+1.hour), final_time: (base_date+2.hours), consultant: consultant, customer: customer)
      expect(appointment.does_not_overlap_other_appoinments?).to be_truthy
      expect(appointment.valid?).to be_truthy
    end
    
    it 'ends when next starts' do      
      appointment=build(:appointment, start_time: (base_date-30.minutes), final_time: base_date, consultant: consultant, customer: customer)
      expect(appointment.does_not_overlap_other_appoinments?).to be_truthy
      expect(appointment.valid?).to be_truthy
    end
    
    it 'starts after' do
      appointment=build(:appointment, start_time: (base_date+30.minutes), final_time: (base_date+2.hours), consultant: consultant, customer: customer)
      expect(appointment.does_not_overlap_other_appoinments?).to be_falsey
      expect(appointment.valid?).to be_falsey
    end
    
    
    
    it 'starts after' do
      appointment=build(:appointment, start_time: (base_date+30.minutes), final_time: (base_date+2.hours), consultant: consultant, customer: customer)
      expect(appointment.does_not_overlap_other_appoinments?).to be_falsey
      expect(appointment.valid?).to be_falsey
    end

    it 'overlaps' do
      appointment=build(:appointment, start_time: (base_date+1.hour+10.minutes), final_time: (base_date+35.minutes), consultant: consultant, customer: customer)
      expect(appointment.does_not_overlap_other_appoinments?).to be_falsey
      expect(appointment.valid?).to be_falsey
    end
    
    it 'contains' do
      appointment=build(:appointment, start_time: (base_date-30.minutes), final_time: (base_date+10.hours), consultant: consultant, customer: customer)
      expect(appointment.does_not_overlap_other_appoinments?).to be_falsey
       expect(appointment.valid?).to be_falsey
    end
    
     it 'same times' do
      appointment=build(:appointment, start_time: (base_date), final_time: (base_date+1.hours), consultant: consultant, customer: customer)
      expect(appointment.does_not_overlap_other_appoinments?).to be_falsey
      expect(appointment.valid?).to be_falsey
      appointment.save
      expect(appointment.persisted?).to be_falsey
    end
    
  end
  
  describe 'not in the past' do
    it 'valid' do
      d = Time.now + 1.hour
      a = build(:appointment, start_time: d, final_time: (d + 1.hour))
      expect(a.valid?).to be_truthy
    end
    
    
    it 'invalid' do
      d = Time.now - 1.hour
      a = build(:appointment, start_time: d, final_time: (d + 1.hour))
      expect(a.valid?).to be_falsey
    end
  end
  
  describe 'update' do
    it 'same start time' do
      d = Time.now + 1.hour
      a = create(:appointment, start_time: d, final_time: (d + 1.hour))
      expect(a.update(final_time: d+ 30.minutes)).to be_truthy
    end
    
    it 'same final time' do
      d = Time.now + 1.hour
      a = create(:appointment, start_time: d, final_time: (d + 1.hour))
      expect(a.update(start_time: d+ 30.minutes)).to be_truthy
    end
    
    it 'invalid time' do
      d = Time.now + 1.hour
      a = create(:appointment, start_time: (d), final_time: (d + 1.hour))
      expect(a.update(start_time: d - 1.day)).to be_falsey
    end
    
  end
  
  
end
