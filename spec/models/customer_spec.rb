require 'spec_helper'

describe Customer do
  describe 'factories' do
    it 'has a valid factory' do
      expect(build(:customer)).to be_valid
    end
    it 'has an invalid factory' do
      expect(build(:invalid_customer)).to_not be_valid
    end
  end
  describe 'attributes' do
    subject { build(:customer) }
    it { should respond_to :first_name }
    it { should respond_to :last_name }
    it { should respond_to :genre }
    it { should respond_to :male? }
    it { should respond_to :female? }
    it { should respond_to :birth_date }
    it { should respond_to :email }
    it { should respond_to :password }
    it { should have_many :appointments }
  end

  describe 'basic validations' do
    subject { build(:customer) }
    it { should validate_presence_of :first_name }
    it { should validate_presence_of :last_name }
    it { should validate_presence_of :email }
    it { should validate_presence_of :password }
    it { should allow_value(nil).for(:genre) }
    it { should allow_value('male').for(:genre) }
    it { should allow_value('female').for(:genre) }
    #it { should validate_uniqueness_of(:email) }
    it { should validate_confirmation_of(:password) }
    it { should allow_value('example@domain.com').for(:email) }
  end

end
