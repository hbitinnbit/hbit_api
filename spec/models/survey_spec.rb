require 'spec_helper'

RSpec.describe Survey, type: :model do
   describe 'factories' do
    it 'has a valid factory' do
      expect(build(:survey)).to be_valid
    end
    it 'has an invalid factory' do
      expect(build(:invalid_survey)).to_not be_valid
    end
  end
  describe 'attributes' do
    subject { build(:survey) }
    it { should respond_to :ranking }
    it { should respond_to :comments }
    it { should belong_to :appointment}
  end
end
