require 'spec_helper'

describe State do
 describe 'factories' do
    it 'has a valid factory' do
      expect(build(:state)).to be_valid
    end
    it 'has an invalid factory' do
      expect(build(:invalid_state)).to_not be_valid
    end
  end
  describe 'attributes' do
    subject { build(:state) }
    it { should respond_to :name }
    it { should respond_to :short_name }
    it { should have_many :cities}
  end
end
