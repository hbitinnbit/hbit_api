require 'csv'
require 'database_cleaner'

desc "Load database tables from csv files"
task :migrate do
  Rake::Task["migrate:specialties"].invoke
  Rake::Task["migrate:states"].invoke
  Rake::Task["migrate:cities"].invoke
  Rake::Task["migrate:consultants"].invoke
end

namespace :migrate do
  desc "Agrega estados"
  task states: :environment do
    migrate "states", "states" do |row|
      estado = State.create(
        name: row['name'],
        short_name: row['short_name']
      )
      puts "Estado: #{estado.name}"
    end
  end

  desc "Agrega ciudades"
  task cities: :environment do
    migrate 'cities', 'cities' do |row|
      estado = State.where(name: row['state']).first
      unless estado.nil?
        ciudad = estado.cities.create(
          name: row['city']
        )
        puts "Ciudad #{ciudad.name} de estado #{estado.name}"
      else
        puts "No se encontró el estado #{row['state']}"
        next
      end
    end
  end

  task specialties: :environment do
    migrate "specialties", "specialties" do |row|
      specialty = Specialty.create(
        description: row['description']
      )
      puts "Especialidad: #{specialty.description}"
    end
  end

  task randomCoordinates: :environment do
    1000.times {
      coords=[rand(19.268238 .. 19.484644), rand(-99.273497..-99.082122)]
      puts "#{coords[0]},#{coords[1]}"
    }
  end

  task consultants: :environment do
    
      ical=File.read(File.join(Rails.root,'spec/fixtures/',"basic.ics"))
    
      #TODO Truncate addresses table
      migrate "consultants", "consultants" do |row|
        consultant = Consultant.create(
          first_name: row['first_name'],
          last_name: row['last_name'],
          email: row['email_contact'],
          password: '12345678',
          email_contact: row['email_contact'],
          phone: row['phone'],
          license: row['license'],
          ranking: row['ranking'],
          specialty_id: row['specialty_id'],
          availability: ical
        )
        
        
        address = Address.create(
          street: row['street'],
          zip: row['zip'],
          latitude: row['latitude'],
          longitude: row['longitude'],
          city_id: row['city_id'],
          consultant_id: consultant.id
        )
        
        puts ">>>>>>>>>>>>>>>>>>>>>>>>"
        if consultant.persisted? 
          puts "Consultant: #{consultant.attributes.except('availability')}"
          puts "Address: #{address.attributes}"
        else
          puts "Consultant: #{consultant.errors.full_messages}"
          puts "Address: #{address.attributes.errors.full_messages}"
        end
        puts "<<<<<<<<<<<<<<<<<<<<<<<<"
        
      end
  end

  
  
  def migrate(entidad,tabla,borrar=true,&block)
    puts "Borra todos los registros de #{entidad}"
    DatabaseCleaner.clean_with(:truncation, only: [tabla]) if borrar

    CSV.foreach(File.join(Rails.root,'db/load/',"#{entidad}.csv"),encoding: 'UTF-8:UTF-8',headers: true) do |row|
      yield row
    end
  end
end
