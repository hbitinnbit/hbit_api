Rails.application.routes.draw do

  #devise_for :customers
  apipie

  root 'apipie/apipies#index'

  mount_devise_token_auth_for 'Customer', at: 'customers',controllers: {
    registrations:      'api/v1/customers/registrations',
    sessions:      'api/v1/customers/sessions'
  }
  

  mount_devise_token_auth_for 'Consultant', at: 'consultants',controllers: {
    registrations:      'api/v1/consultants/registrations',
    sessions:      'api/v1/consultants/sessions'
  }
  
  as :consultant do
    # Define routes for Consultant within this block.
  end

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      namespace :consultants do
        resources :specialties, only: [:index, :update]
        #put 'specialty', to: 'specialties#update'
        #patch 'specialty', to: 'specialties#update'   
        
        resources :appointments, only: [:index, :update]
        
        resources :states, only: [:index] do
          member do
            get :cities
          end
        end
        
        put 'address', to: 'addresses#update', :as => 'address'
        patch 'address', to: 'addresses#update'
        put 'address/coordinates', to: 'addresses#coordinates', :as => 'coordinates'
        patch 'address/coordinates', to: 'addresses#coordinates'
        put 'avatar', to: 'avatars#update', :as => 'avatar'
        put 'availability', to: 'availability#update', :as => 'availability'
        get 'availability', to: 'availability#show'
        resources :customers, only: [:show]
      end  
      
      
    
      namespace :customers do
        put 'avatar', to: 'avatars#update', :as => 'avatar'

        resources :appointments, only: [:index] do
          member do
            post :surveys
          end
        end
        
        concern :paginatable do
          get '(page/:page)(/per/:per)', :action => :index, :on => :collection, :as => ''
        end
        resources :consultants, only: [:index, :show], :concerns => :paginatable do
          resources :appointments, only: [:create, :update, :destroy, :index]
          get :availability, :on => :member
          collection do
            get :locations  
          end
        end
      end
    end
  end

# The priority is based upon order of creation: first created -> highest priority.
# See how all your routes lay out with "rake routes".

# You can have the root of your site routed with "root"
# root 'welcome#index'

# Example of regular route:
#   get 'products/:id' => 'catalog#view'

# Example of named route that can be invoked with purchase_url(id: product.id)
#   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

# Example resource route (maps HTTP verbs to controller actions automatically):
#   resources :products

# Example resource route with options:
#   resources :products do
#     member do
#       get 'short'
#       post 'toggle'
#     end
#
#     collection do
#       get 'sold'
#     end
#   end

# Example resource route with sub-resources:
#   resources :products do
#     resources :comments, :sales
#     resource :seller
#   end

# Example resource route with more complex sub-resources:
#   resources :products do
#     resources :comments
#     resources :sales do
#       get 'recent', on: :collection
#     end
#   end

# Example resource route with concerns:
#   concern :toggleable do
#     post 'toggle'
#   end
#   resources :posts, concerns: :toggleable
#   resources :photos, concerns: :toggleable

# Example resource route within a namespace:
#   namespace :admin do
#     # Directs /admin/products/* to Admin::ProductsController
#     # (app/controllers/admin/products_controller.rb)
#     resources :products
#   end
end
